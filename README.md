# BOINC@HOME@K8S

If you need some image to test your K8s deployments, or just if you'd like to get some fun playing around with your cluster, then this repo is for you.

Also, you ma help saving lifes with this one.

## Configure me

All you need to do is to pass two envvars:

* ENV BOINC_PROJECT_URL (eg. https://boinc.bakerlab.org/rosetta)
* ENV BOINC_PROJECT_AUTH

## But how do I obtain account details, especially BOINC_PROJECT_AUTH?

Create your account with BOINC@Home

boinc_cmd --lookup_account https://boinc.bakerlab.org/ <Email> <Password>

You may execute it from within working container. It will bootup without credentials provided, it will simply fail to attach to the project.