## File to build docker image for Boinc
#
# Forked from  ozzyjohnson/boinc with original MAINTAINER Ozzy Johnson <docker@ozzy.io>
# I'd advise pulling from that repo for most up to date version for this.
# I'm using and forking this over mainly as an example for kubernetes.
#
# See https://github.com/ckleban/boinc-on-k8/ for original work.

FROM debian:buster

MAINTAINER Jan Machalica <jan.machalica@gmail.com>

# Update and install minimal.
RUN apt-get update

RUN apt-get install \
         --yes \
         --no-install-recommends \
         --no-install-suggests \
       boinc-client

RUN  apt-get clean

RUN  rm -rf /var/lib/apt/lists/*

# Data volume.
ONBUILD VOLUME /data

# Getting ready.
WORKDIR /data

COPY entry.sh /

RUN chmod +x /entry.sh

ENV DEBIAN_FRONTEND noninteractive
ENV BOINC_PROJECT_URL  https://boinc.bakerlab.org/rosetta
ENV BOINC_PROJECT_AUTH $BOINC_PROJECT_AUTH

# Default command.
ENTRYPOINT  ["/entry.sh"]
